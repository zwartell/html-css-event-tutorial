## HTML-CSS-Event-Tutorial

### Author: Zachary Wartell

HTML-CSS-Event-Tutorial is gradable tutorial covering minimal basics of HTML, CSS and Event handling.  The tutorial is designed to supplement the https://sites.google.com/site/webglbook/ used in Dr. Wartell's computer graphics course and to cover pre-requisite knowledge for the course projects.

Web Site:  https://webpages.charlotte.edu/zwartell/Teaching/html-css-event-tutorial/ITCS_x120_HTML-CSS-Event_Tutorial.html

#### Utilized Libraries

- Zachary Wartell. zjw-generic-instructions. https://gitlab.com/zwartell/zjw-generic-instructions.git. Via git submodule.
- JQuery. https://jquery.com/. Via git submodule.